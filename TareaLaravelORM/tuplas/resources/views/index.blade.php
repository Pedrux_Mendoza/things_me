<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th>Categoria</th>
				<th>Producto</th>
			</tr>
		</thead>
		<tbody>
			@foreach($facturas as $row)
				<tr>
					<td>{{$row->nombre}}</td>
					<td>{{$row->producto}}</td>
				</tr>
            @endforeach
		</tbody>
	</table>
</body>
</html>