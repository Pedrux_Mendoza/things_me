<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'factura';

    /**
    * The primary key associated with the table.
    *
    * @var string
    */
    protected $primaryKey = 'num_factura';      
}
