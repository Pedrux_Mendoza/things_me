<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'producto';

    /**
    * The primary key associated with the table.
    *
    * @var string
    */
    protected $primaryKey = 'id_producto';

    /**
    * Indicates if the IDs are auto-incrementing.
    *
    * @var bool
    */
    public $incrementing = false;    
}
