<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModoPago extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'modo_pago';

    /**
    * The primary key associated with the table.
    *
    * @var string
    */
    protected $primaryKey = 'num_pago';

    /**
    * Indicates if the IDs are auto-incrementing.
    *
    * @var bool
    */
    //Si deseas usar una clave primaria que no sea de autoincremeneto o numérica debes establecer la propiedad pública $incrementing de tu modelo a false.
    public $incrementing = false;    
}
