<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'cliente';

    /**
    * The primary key associated with the table.
    *
    * @var string
    */
    protected $primaryKey = 'id_cliente';    
}
