<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Queries\FacturasQueries;

class Invoice extends Controller
{
	protected $FacturasQueries;

    public function __construct(FacturasQueries $FacturasQueries)
    {
    	$this->FacturasQueries = $FacturasQueries;
    }

    public function index()
    {
    	$facturas = $this->FacturasQueries->getFacturas();
    	return view('index', compact('facturas'));
    }
}
