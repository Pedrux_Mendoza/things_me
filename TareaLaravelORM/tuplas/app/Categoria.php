<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    //Observa que no le dijimos a Eloquent cuál tabla usar para nuestro modelo Flight. Por convención, el nombre de la clase en plural y en formato "snake_case" será usado como el nombre de tabla a menos que otro nombre sea especificado expresamente.Puedes especificar una tabla personalizada al definir una propiedad table en tu modelo.    
    protected $table = 'categoria';

    /**
    * The primary key associated with the table.
    *
    * @var string
    */
    //Eloquent asumirá que cada tabla tiene una columna de clave primaria denominada id. Puedes definir una propiedad $primaryKey protegida para sobrescribir esta convención.    
    protected $primaryKey = 'id_categoria';    
}
