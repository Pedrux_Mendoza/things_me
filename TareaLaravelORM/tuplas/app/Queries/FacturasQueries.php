<?php
//Especifico la ruta donde se encuentra este archivo
namespace App\Queries;
use App\Categoria;
//Nombre de la clase
class FacturasQueries{
//Metodos que llamaran a mis consultas DB
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getFacturas()
    {
        return Categoria::join('producto', 'producto.id_categoria', '=', 'categoria.id_categoria')->select('categoria.nombre', 'producto.nombre as producto')->where('categoria.id_categoria', 13)->get();;
    }
}