<?php

use Illuminate\Database\Seeder;
use App\Producto;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::table('producto')->insert([
    		"id_producto" => 4,
            "nombre" => "Costillas",
            "precio" => 1.50,
            "stock" => 44,
            "id_categoria" => 4
        ]);

    	\DB::table('producto')->insert([
    		"id_producto" => 5,
            "nombre" => "Lonja de res",
            "precio" => 2.75,
            "stock" => 35,
            "id_categoria" => 9
        ]);

    	\DB::table('producto')->insert([
    		"id_producto" => 6,
            "nombre" => "Gaseosa",
            "precio" => 1.85,
            "stock" => 40,
            "id_categoria" => 13
        ]);

    	\DB::table('producto')->insert([
    		"id_producto" => 7,
            "nombre" => "Jugo Del Valle",
            "precio" => 1.50,
            "stock" => 60,
            "id_categoria" => 13
        ]);

    	\DB::table('producto')->insert([
    		"id_producto" => 8,
            "nombre" => "Lasagna",
            "precio" => 1.25,
            "stock" => 50,
            "id_categoria" => 11
        ]);

    	\DB::table('producto')->insert([
    		"id_producto" => 9,
            "nombre" => "Camisa Polo",
            "precio" => 5,
            "stock" => 30,
            "id_categoria" => 8
        ]);

    	\DB::table('producto')->insert([
    		"id_producto" => 10,
            "nombre" => "Funko Pop",
            "precio" => 15,
            "stock" => 25,
            "id_categoria" => 7
        ]);                                               

        \DB::table('producto')->insert([
        	"id_producto" => 11,
            "nombre" => "HotWheels",
            "precio" => 1.50,
            "stock" => 50,
            "id_categoria" => 7
        ]);

        \DB::table('producto')->insert([
        	"id_producto" => 12,
            "nombre" => "Leche",
            "precio" => 2.50,
            "stock" => 50,
            "id_categoria" => 10
        ]);

        \DB::table('producto')->insert([
        	"id_producto" => 13,
            "nombre" => "Pie de Manzana",
            "precio" => 1.50,
            "stock" => 15,
            "id_categoria" => 6
        ]);          
    }
}
