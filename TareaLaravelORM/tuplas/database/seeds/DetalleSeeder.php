<?php

use Illuminate\Database\Seeder;

class DetalleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	  	\DB::table('detalle')->insert([
            "num_factura" => 3,
            "id_producto" => 5,
            "cantidad" => 1,
            "precio" => 2.75
        ]);

		\DB::table('detalle')->insert([
            "num_factura" => 8,
            "id_producto" => 8,
            "cantidad" => 3,
            "precio" => 3.75
        ]);

		\DB::table('detalle')->insert([
            "num_factura" => 10,
            "id_producto" => 6,
            "cantidad" => 6,
            "precio" => 11.1
        ]);

		\DB::table('detalle')->insert([
            "num_factura" => 10,
            "id_producto" => 10,
            "cantidad" => 9,
            "precio" => 135
        ]);

		\DB::table('detalle')->insert([
            "num_factura" => 2,
            "id_producto" => 10,
            "cantidad" => 1,
            "precio" => 15
        ]);

		\DB::table('detalle')->insert([
            "num_factura" => 9,
            "id_producto" => 8,
            "cantidad" => 2,
            "precio" => 2.50
        ]);

\DB::table('detalle')->insert([
            "num_factura" => 8,
            "id_producto" => 4,
            "cantidad" => 3,
            "precio" => 4.50
        ]);  

        \DB::table('detalle')->insert([
            "num_factura" => 7,
            "id_producto" => 7,
            "cantidad" => 20,
            "precio" => 30
        ]);

		\DB::table('detalle')->insert([
            "num_factura" => 9,
            "id_producto" => 7,
            "cantidad" => 4,
            "precio" => 6
        ]);         

        \DB::table('detalle')->insert([
            "num_factura" => 5,
            "id_producto" => 5,
            "cantidad" => 10,
            "precio" => 27.5
        ]);                                                  
    }
}
