<?php

use Illuminate\Database\Seeder;
use App\ModoPago;

class ModoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('modo_pago')->insert([
            "num_pago" => 3,
            "nombre" => "efectivo",
            "otros_detalles" => "Pagos con efectivo"
        ]);

        \DB::table('modo_pago')->insert([
            "num_pago" => 4,
            "nombre" => "tarjeta de credito",
            "otros_detalles" => "Pagos con tarjeta de credito"
        ]);

        \DB::table('modo_pago')->insert([
            "num_pago" => 5,
            "nombre" => "tarjeta de regalo",
            "otros_detalles" => "Pagos con tarjeta de regalo"
        ]);

        \DB::table('modo_pago')->insert([
            "num_pago" => 6,
            "nombre" => "cheque",
            "otros_detalles" => "Pagos con cheque"
        ]);   	    	    	    
    }
}
