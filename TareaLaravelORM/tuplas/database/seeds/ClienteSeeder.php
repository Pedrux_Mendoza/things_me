<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
/*use App\Cliente;*/

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cliente')->insert([
            "nombre" => "Julie",
            "apellido" => "Griffin",
            "direccion" => "Col. San Jose, Santa Tecla",
            "fecha_nacimiento" => "1989-03-03",
            "telefono" => "2221-5689",
            "email" => "fames@libero.org"
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Rebeca",
            "apellido" => "Price",
            "direccion" => "Los olivos"
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Lydia",
            "apellido" => "Mercado",
            "direccion" => "Cuscatancingo"
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Alexis",
            "apellido" => "Valencia",
            "direccion" => "6852 Urna, Avenue",
            "fecha_nacimiento" => "1993-01-15",
            "telefono" => "7643-9638",
            "email" => "Integer.mollis.Integer@estNunc.org"
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Alfonso",
            "apellido" => "Payne",
            "direccion" => "Ap #987-3368 Ut Avenue",
            "fecha_nacimiento" => "1980-04-18",
            "telefono" => "2323-9512",
            "email" => "neque@SuspendisseduiFusce.org"
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Ruby",
            "apellido" => "Conrad",
            "direccion" => "182-9302 Consequat Street",
            "fecha_nacimiento" => "1999-06-30",
            "telefono" => "2747-5258",
            "email" => "iaculis@ac.com"
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Mohammad",
            "apellido" => "Cote",
            "direccion" => "2867 Tristique Av."
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Arsenio",
            "apellido" => "Arsenio",
            "direccion" => "P.O. Box 811, 880 Magna. Street",
            "fecha_nacimiento" => "1973-02-20",
            "telefono" => "7722-8150",
            "email" => "lacus.vestibulum@molestietellus.edu"
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Beatrice",
            "apellido" => "Owen",
            "direccion" => "654-733 Massa Road",
            "fecha_nacimiento" => "1969-08-12",
            "telefono" => "2404-4055",
            "email" => "tincidunt@ipsum.org"
        ]);

        \DB::table('cliente')->insert([
            "nombre" => "Courtney",
            "apellido" => "Garcia",
            "direccion" => "Ap #450-3624 Scelerisque St."
        ]);           
    }
}
