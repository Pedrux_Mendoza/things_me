<?php

use Illuminate\Database\Seeder;

class FacturaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::table('factura')->insert([
    		"codigo" => "PHP4632",
            "id_cliente" => 6,
            "fecha" => "2019-12-28",
            "num_pago" => 1
        ]);

    	\DB::table('factura')->insert([
    		"codigo" => "PHP0942",
            "id_cliente" => 4,
            "fecha" => "2019-12-13",
            "num_pago" => 2
        ]);

    	\DB::table('factura')->insert([
    		"codigo" => "PHP5686",
            "id_cliente" => 8,
            "fecha" => "2019-11-30",
            "num_pago" => 6
        ]);

    	\DB::table('factura')->insert([
    		"codigo" => "PHP1928",
            "id_cliente" => 2,
            "fecha" => "2019-12-23",
            "num_pago" => 2
        ]);

    	\DB::table('factura')->insert([
    		"codigo" => "PHP1231",
            "id_cliente" => 7,
            "fecha" => "2019-11-16",
            "num_pago" => 3
        ]);

        \DB::table('factura')->insert([
    		"codigo" => "PHP0399",
            "id_cliente" => 9,
            "fecha" => "2019-11-12",
            "num_pago" => 4
        ]);

		\DB::table('factura')->insert([
    		"codigo" => "PHP4221",
            "id_cliente" => 10,
            "fecha" => "2019-11-18",
            "num_pago" => 1
        ]);

		\DB::table('factura')->insert([
    		"codigo" => "PHP9967",
            "id_cliente" => 9,
            "fecha" => "2019-12-12",
            "num_pago" => 5
        ]);

		\DB::table('factura')->insert([
    		"codigo" => "PHP7144",
            "id_cliente" => 5,
            "fecha" => "2019-12-30",
            "num_pago" => 5
        ]);

		\DB::table('factura')->insert([
    		"codigo" => "PHP2868",
            "id_cliente" => 3,
            "fecha" => "2019-11-02",
            "num_pago" => 6
        ]);           
    }
}
