<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
          $this->call([
            ModoPagoSeeder::class,
            CategoriaSeeder::class,
            ClienteSeeder::class,
            ProductoSeeder::class,
            FacturaSeeder::class,
            DetalleSeeder::class,
        ]);
    }
}
