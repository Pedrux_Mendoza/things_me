<?php

use Illuminate\Database\Seeder;
use App\Categoria;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categoria')->insert([
            "nombre" => "Embutidos",
            "descripcion" => "Derivados del cerdo"
        ]);

        \DB::table('categoria')->insert([
            "nombre" => "Deportivo",
            "descripcion" => "Para mantenerse saludable"
        ]);  
        
        \DB::table('categoria')->insert([
            "nombre" => "Pasteleria",
            "descripcion" => "Ricos y deliciosos pasteles"
        ]);

        \DB::table('categoria')->insert([
            "nombre" => "Juguetes",
            "descripcion" => "Juegos de mesas y mas"
        ]);

        \DB::table('categoria')->insert([
            "nombre" => "Ropa",
            "descripcion" => "Para vestirse mejor"
        ]);

        \DB::table('categoria')->insert([
            "nombre" => "Carnes",
            "descripcion" => "Cortes y mas"
        ]);

        \DB::table('categoria')->insert([
            "nombre" => "Lacteos",
            "descripcion" => "Derivados de la vaca"
        ]);

        \DB::table('categoria')->insert([
            "nombre" => "Pasta",
            "descripcion" => "Comida Italiana"
        ]);

        \DB::table('categoria')->insert([
            "nombre" => "Verduras",
            "descripcion" => "Vegetales Frescos"
        ]);

        \DB::table('categoria')->insert([
            "nombre" => "Bebidas",
            "descripcion" => "Para refrescarse"
        ]);  	   	
    }
}
