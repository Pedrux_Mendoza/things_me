<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::get('/factura', 'Invoice@index');
Route::post('/ajaxClientes', 'Invoice@ajaxDailyCustomer');
Route::get('/ajaxgetClientes', 'Invoice@ajaxGetCustomer');
Route::post('/ajaxCreditos', 'Invoice@ajaxCreditCustomer');
Route::post('/ajaxClienteNuevo', 'Invoice@ajaxNewCustomer');
Route::post('/ajaxPrecio', 'Invoice@ajaxPricesProductos');
Route::post('/ajaxStock', 'Invoice@ajaxStockProductos');
Route::post('/ajaxAgregar', 'Invoice@ajaxAddProducts');
Route::get('/carrito', 'Invoice@carrito');
Route::post('/ajaxRemover', 'Invoice@remover');
Route::post('/facturar1', 'Invoice@facturarContado');
Route::get('/home', 'HomeController@index')->name('home');
