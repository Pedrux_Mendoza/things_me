<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
  <link href="{{ asset('css/chart.css') }}" rel="stylesheet">
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/scripts.js') }}"></script>  
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
      // Cargar gráficos y el paquete barchart.        
      google.charts.load('current', {'packages':['bar']});
      // Dibuje el gráfico circular para la pizza de Sarah cuando se carguen los Gráficos.
      google.charts.setOnLoadCallback(drawSarahChart);
      // Dibuje el gráfico circular para la pizza de Anthony cuando se carguen los Gráficos..
      google.charts.setOnLoadCallback(drawAnthonyChart);

      // Dibuja el gráfico circular de la pizza de Sarah.
      function drawSarahChart() {
        var data = new google.visualization.arrayToDataTable([
          ['Clientes', 'No. de Veces'],
          ['Mushrooms', 3],
          ['Onions', 1],
          ['Olives', 1],
          ['Zucchini', 1],
          ['Pepperoni', 2]
          ]);

        // Establecer opciones para el gráfico de barras de Sarah..
        var options = {
          title: 'Clientes al Contado',
          colors: ['silver'],
          width: '100%',
          legend: { position: 'none' },
          chart: { title: 'Clientes',
          subtitle: 'Que mas pagan al contado' },
          bars: 'horizontal', // Required for Material Bar Charts.
          axes: {
            x: {
              0: { side: 'top', label: 'No. de Veces'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        // Instanciamos y dibujamos la tabla para la pizza de Sarah.
        var chart = new google.charts.Bar(document.getElementById('Sarah_chart_div'));
        chart.draw(data, options);
      };

      // Dibuja el gráfico circular de la pizza de Anthony.
      function drawAnthonyChart() {
        var data = new google.visualization.arrayToDataTable([
          ['Clientes', 'No. de Veces'],
          ['Mushrooms', 2],
          ['Onions', 2],
          ['Olives', 2],
          ['Zucchini', 0],
          ['Pepperoni', 3]
          ]);

        // Establecer opciones para el gráfico de barras de Anthony.
        var options = {
          title: 'Clientes al Credito',
          colors: ['silver'],
          width: '100%',
          legend: { position: 'none' },
          chart: { title: 'Clientes',
          subtitle: 'Que mas pagan al credito' },
          bars: 'horizontal', // Required for Material Bar Charts.
          axes: {
            x: {
              0: { side: 'top', label: 'No. de Veces'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        // Instanciamos y dibujamos la tabla para la pizza de Anthony.
        var chart = new google.charts.Bar(document.getElementById('Anthony_chart_div'));
        chart.draw(data, options);
      };      
    </script>
    <script type="text/javascript">
      $(window).resize(function(){
        drawSarahChart();
        drawAnthonyChart();
      });
    </script>  
  </head>
  <body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
      <a class="navbar-brand" href="{{ asset('/home') }}">Cool Sales</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
        >
      </form>
      <!-- Navbar-->
      <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <ul class="navbar-nav ml-auto ml-md-0">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>                        
            </div>
          </li>
        </ul>
      </div>
    </nav>      
    <div id="layoutSidenav">
      <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
          <div class="sb-sidenav-menu">
            <div class="nav">
              <div class="sb-sidenav-menu-heading">Cajero</div>
              <a class="nav-link" href="{{ asset('/home') }}"
              ><div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
              Inicio</a
              >
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts"
              ><div class="sb-nav-link-icon"><i class="fas fa-receipt"></i></div>
              Facturas
              <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                ></a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                  <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="{{ asset('/factura') }}">Generar Facturas</a><a class="nav-link" href="#">Mostrar</a></nav>
                </div>
              </div>
            </div>
            <div class="sb-sidenav-footer">
              <div class="small">Logged in as:</div>
              {{ Auth::user()->name }}
            </div>
          </nav>
        </div>
        <div id="layoutSidenav_content">
          <main>
            <div class="container-fluid">
              <h1 class="mt-4">Inicio</h1>
              <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Inicio</li>
              </ol>
              <div class="row">
                <div class="col-xl-6">
                  <div class="card mb-4">
                    <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Top Clientes Credito</div>
                    <div class="card-body"><div id="Anthony_chart_div" class="chart"></div></div>
                  </div>
                </div>
                <div class="col-xl-6">
                  <div class="card mb-4">
                    <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Top Clientes Contado</div>
                    <div class="card-body"><div id="Sarah_chart_div" class="chart"></div></div>
                  </div>
                </div>
              </div>
            </div>
          </main>
          <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
              <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2019</div>
                <div>
                  <a href="#">Privacy Policy</a>
                  &middot;
                  <a href="#">Terms &amp; Conditions</a>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>          
    </body>
    </html>