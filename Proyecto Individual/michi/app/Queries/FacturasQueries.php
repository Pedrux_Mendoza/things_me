<?php
//Especifico la ruta donde se encuentra este archivo
namespace App\Queries;
//Nombre de la clase
class FacturasQueries{
//Metodos que llamaran a mis consultas DB
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getModoPago()
    {
        return \DB::table('modopago')->select('idModoPago', 'nombre_pago')->get();
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getTipoPago()
    {
        return \DB::table('tipopago')->select('idTipoPago', 'NombreTipoPago')->get();
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getClientes()
    {
        return \DB::table('clientes')->select('id_cliente', 'Nombre_Completo')->get();
    }
    /*Fn: Verifica el usuario ingresado
    @param: usuario, contraseña
    @return: conteo de usuarios que se encuentra en la DB*/
    public function getClientesID($id)
    {
        return \DB::table('clientes')->select('DUI', 'NIT', 'Telefono')->where('id_cliente', '=', $id)->get();
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getTarjetas()
    {
        return \DB::table('tarjetas')->select('idTarjetas', 'NombreTarjetas')->get();
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getProductos()
    {
        return \DB::table('productos')->select('idProducto', 'NombreProducto')->get();
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getClientesCreditos($id)
    {
        return \DB::table('clientes')->join('creditos', 'creditos.idCreditos', '=', 'clientes.idCredito')->select('creditos.Saldo_Actual', 'creditos.CuotaMensual', 'creditos.Dias_Credito')->where('id_cliente', '=', $id)->get();
    }
    /*Fn: Verifica si el id existe en la tabla hobbie
    @param: idhobbie
    @return: Valor booleano si existe en la DB*/    
    public function verificarID($id)
    {
        $fact = \DB::table('factura')->where('idFactura', '=', $id)->get();
        if ($fact->count() > 0) {
            return "true";
        }else{
            return "false";
        }
    }
    /*Fn: Inserta una aficion con su JOven
    @param: nombre de la aficion y el joven
    @return: Insecion de Aficiones en la DB*/
    public function insertCliente($dui, $nit, $nombre, $telefono)
    {
        return \DB::table('clientes')->insert(['DUI' => $dui, 'NIT' => $nit, 'Nombre_Completo' => $nombre, 'Telefono'=> $telefono]);
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getPricesProductos($id)
    {
        return \DB::table('productos')->select('PrecioPublico','Stock')->where('idProducto', '=', $id)->get();
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getStockProductos($id)
    {
        return \DB::table('productos')->select('Stock')->where('idProducto', '=', $id)->get();
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function getProductoID($id)
    {
        return \DB::table('productos')->join('categorias', 'categorias.idCategoria', '=', 'productos.idCategoria')->select('productos.CodigoProducto', 'productos.NombreProducto', 'categorias.NombreCategoria')->where('productos.idProducto', '=', $id)->get();
    }
    /*Fn: Inserta una aficion con su JOven
    @param: nombre de la aficion y el joven
    @return: Insecion de Aficiones en la DB*/
    public function insertFacturaContado($cliente, $tipo, $modo, $empleado, $fecha, $total)
    {
        \DB::table('factura')->insert(['idCliente' => $cliente, 'idTPago' => $tipo, 'idEmpleado' => $empleado, 'Fecha' => $fecha, 'Total_Pago' => $total, 'idModoPago' => $modo]);

        $last_id = \DB::getPDO()->lastInsertId();

        return $last_id;
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function insertDetalles($factura, $producto, $cantidad, $precio)
    {
        return \DB::table('detalles_pf')->insert(['idFactura' => $factura, 'idProducto' => $producto, 'Cantidad' => $cantidad, 'Precio' => $precio]);
    }
    /*Fn: Devuelve un listado de Jovenes
    @param: na
    @return: Listados de Jovenes en DB*/
    public function updateCredito($cliente, $saldo)
    {
        $idCredit = \DB::table('clientes')->select('idCredito')->where('id_cliente', '=', $cliente)->get();

        foreach ($idCredit as $row) {
            $id = $row->idCredito;
        }

        return \DB::table('creditos')->where('idCreditos', '=', $id)->update(['Saldo_Actual' => $saldo]);
    }
    /*Fn: Inserta una aficion con su JOven
    @param: nombre de la aficion y el joven
    @return: Insecion de Aficiones en la DB*/
    public function insertFacturaCredito($cliente, $tipo, $empleado, $fecha, $total)
    {
        \DB::table('factura')->insert(['idCliente' => $cliente, 'idTPago' => $tipo, 'idEmpleado' => $empleado, 'Fecha' => $fecha, 'Total_Pago' => $total]);

        $last_id = \DB::getPDO()->lastInsertId();

        return $last_id;
    }    
}