<?php

namespace App\Http\Controllers;
//LLAMANDO A MI CLASE FACTURASQUERIES
use App\Queries\FacturasQueries;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Cart;

class Invoice extends Controller
{
	//Se almacenará en una propiedad  heredable.
	protected $FacturasQueries;	
    //En el método constructor, se pasará para poder utilizarlo cuando desee dentro de la clase.
    /*Fn: Constructor de la Clase
    @param: Objeto de tipo FacturasQueries
    @return: na */    
    public function __construct(FacturasQueries $FacturasQueries)
    {
    	$this->FacturasQueries = $FacturasQueries;
    }    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $corre='';
        $contador = 0;
        do  {
            $contador++;
            $resp = $this->FacturasQueries->verificarID($contador);

            if ($contador > 0 AND $contador <= 9)
            {
                $corre =  'PHP000'.$contador;
            }
            else if($contador >= 10 AND $contador <= 99)
            {
                $corre =  'PHP00'.$contador;
            }
            else if($contador >= 100 AND $contador <= 999)
            {
                $corre =  'PHP0'.$contador;
            }
            else if($contador >= 1000 AND $contador <= 9999)
            {
                $corre =  'PHP'.$contador;
            }

        }while($resp == "true");
        $modo = $this->FacturasQueries->getModoPago();
        $tipo = $this->FacturasQueries->getTipoPago();
        $cliente = $this->FacturasQueries->getClientes();
        $tarjeta = $this->FacturasQueries->getTarjetas();
        $productos = $this->FacturasQueries->getProductos();
        $items = Cart::getContent();
        return view('factura.factura', compact(['modo', 'tipo', 'cliente', 'tarjeta', 'productos', 'corre', 'items']));
    }

    public function carrito()
    {
        $items = Cart::getContent();
        return view('factura.carrito', compact(['items']));
    }        

    public function ajaxDailyCustomer(Request $request)
    {
    	$id = $request->id;
    	$customer = $this->FacturasQueries->getClientesID($id);
    	foreach ($customer as $row) {
    		$array = array('dui' => $row->DUI, 'nit' => $row->NIT, 'tel' => $row->Telefono);
    	}
        if (empty($array)) 
        {
            $array = array('vacio' => "true");
            return response()->json($array);
        }
        else{
            return response()->json($array);
        }        
    }    

    public function ajaxCreditCustomer(Request $request)
    {
        $id = $request->id;
        $credit = $this->FacturasQueries->getClientesCreditos($id);
        foreach ($credit as $row) {
            $meses = $row->Dias_Credito * 0.0328549112;
            $subtot = $row->CuotaMensual * $meses;
            $saldo = $row->Saldo_Actual - $subtot;
            $array = array('saldo' => $row->Saldo_Actual, 'cuota' => $row->CuotaMensual, 'dias' => $row->Dias_Credito, 'disponible' => round($saldo, 2));
        }
        if (empty($array)) 
        {
            $array = array('vacio' => "true");
            return response()->json($array);
        }
        else{
            return response()->json($array);
        }        
    }

    public function ajaxNewCustomer(Request $request)
    {
        $nombre = $request->nombre;
        $dui = $request->dui;
        $nit = $request->nit;
        $telefono = $request->tel;
        $msj = $this->FacturasQueries->insertCliente($dui, $nit, $nombre, $telefono);
        $array = array('msj' => "success");
        return response()->json($array);        
    }

    public function ajaxGetCustomer()
    {
        $customers = $this->FacturasQueries->getClientes();
        $array = $customers->toArray();
        return response()->json($array);     
    }

    public function ajaxPricesProductos(Request $request)
    {
        $id = $request->id;
        $prices = $this->FacturasQueries->getPricesProductos($id);
        foreach ($prices as $row) {
            $array = array('precio' => $row->PrecioPublico, 'stock' => $row->Stock);
        }
        if (empty($array)) 
        {
            $array = array('vacio' => "true");
            return response()->json($array);
        }
        else{
            return response()->json($array);
        }        
    }

    public function ajaxStockProductos(Request $request)
    {
        $id = $request->id;
        $prices = $this->FacturasQueries->getStockProductos($id);
        foreach ($prices as $row) {
            $array = array('stock' => $row->Stock);
        }
        if (empty($array)) 
        {
            $array = array('vacio' => "true");
            return response()->json($array);
        }
        else{
            return response()->json($array);
        }        
    }

    public function ajaxAddProducts(Request $request)
    {
        $id = $request->id;
        $cantidad = $request->cantidad;
        $precio = $request->precio;
        $producto = $this->FacturasQueries->getProductoID($id);
        foreach ($producto as $row) {
            Cart::add(array(
                'id' => $id,
                'name' => $row->NombreProducto,
                'price' => $precio,
                'quantity' => $cantidad,
                'attributes' => array(
                    'categoria' => $row->NombreCategoria,
                    'codigo' => $row->CodigoProducto
                )
            ));        
        }
        $subtotal = \Cart::getTotal();
        $iva = $subtotal * 0.13;
        $total = $subtotal + $iva;
        $array = array('total' => $total);
        return response()->json($array);        
    }

    public function remover(Request $request){
        $id = $request->id;
        Cart::remove($id);
        $subtotal = \Cart::getTotal();
        $iva = $subtotal * 0.13;
        $total = $subtotal + $iva;        
        $array = array('total' => $total);
        return response()->json($array);  
    }

    public function facturarContado(Request $request){

        $cliente = $request->selecli;
        $tipo = $request->pagos;
        $modo = $request->modo;
        $fecha = $request->fechaactual;
        $empleado = $request->idemp;
        //Contado
        $totapago = $request->totals;
        //Credito
        $dias = $request->diascredit;
        $cuota = $request->cuota;
        $saldo_act = $request->limitcredit;       

        if ($tipo == 1) {
            $meses = $dias * 0.0328549112;
            $subtot = $cuota * $meses;
            $iva = $subtot * 0.13;
            $total = $subtot + $iva; 
            $saldo = $saldo_act - $total;

            $factcredit = $this->FacturasQueries->insertFacturaCredito($cliente, $tipo, $empleado, $fecha, round($total, 2));

            $this->FacturasQueries->updateCredito($cliente, round($saldo, 2));

            $items = Cart::getContent();

            foreach ($items as $row) {
                $this->FacturasQueries->insertDetalles($factcredit, $row->id, $row->quantity, $row->getPriceSum());
            }

        }elseif ($tipo == 2) {
            $factcont = $this->FacturasQueries->insertFacturaContado($cliente, $tipo, $modo, $empleado, $fecha, $totapago);

            $items = Cart::getContent();

            foreach ($items as $row) {
                $this->FacturasQueries->insertDetalles($factcont, $row->id, $row->quantity, $row->getPriceSum());
            }            
        }

        Cart::clear();

        return redirect()->action('Invoice@index');
    }    

}
