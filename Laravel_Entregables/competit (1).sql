-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-02-2020 a las 23:52:38
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `competit`
--
CREATE DATABASE IF NOT EXISTS `competit` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `competit`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_user_competition` ()  BEGIN
SELECT u.usuario_id, u.nombres, u.apellidos, com.nombre AS competencia FROM usuarios u
INNER JOIN detalle_cp dcp ON dcp.us_id = u.usuario_id
INNER JOIN competencias com ON com.competencia_id = dcp.competencia_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_UC` (IN `pa_nombres` VARCHAR(20), IN `pa_apellidos` VARCHAR(30), IN `pa_movil` CHAR(9), IN `pa_correo` VARCHAR(90), IN `pa_nombre` VARCHAR(30), IN `pa_descripcion` VARCHAR(50))  BEGIN
DECLARE usuario INT;
DECLARE competencia INT;
INSERT INTO `usuarios`(`nombres`, `apellidos`, `movil`, `correo`) VALUES (pa_nombres,pa_apellidos,pa_movil,pa_correo);

SET usuario = LAST_INSERT_ID();

INSERT INTO `competencias`(`nombre`, `descripcion`) VALUES (pa_nombre,pa_descripcion);

SET competencia = LAST_INSERT_ID();

INSERT INTO `detalle_cp`(`us_id`, `competencia_id`) VALUES (usuario,competencia);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencias`
--

CREATE TABLE `competencias` (
  `competencia_id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `us_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `competencias`
--

INSERT INTO `competencias` (`competencia_id`, `nombre`, `descripcion`, `us_id`) VALUES
(1, 'Trabajo en equipo', 'Requiere trabajar en proyectos a varios empleados', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cp`
--

CREATE TABLE `detalle_cp` (
  `id_detalle` int(11) NOT NULL,
  `us_id` int(11) NOT NULL,
  `competencia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_cp`
--

INSERT INTO `detalle_cp` (`id_detalle`, `us_id`, `competencia_id`) VALUES
(1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario_id` int(11) NOT NULL,
  `nombres` varchar(20) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `movil` char(9) NOT NULL,
  `correo` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario_id`, `nombres`, `apellidos`, `movil`, `correo`) VALUES
(1, 'Pedro', 'Mendoza', '7730-1602', 'pedrux_mendoza@hotmail.com'),
(2, 'Michelle', 'De Mendoza', '1234-5678', 'michelle.demendoza@gmail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `competencias`
--
ALTER TABLE `competencias`
  ADD PRIMARY KEY (`competencia_id`);

--
-- Indices de la tabla `detalle_cp`
--
ALTER TABLE `detalle_cp`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `fk_competencias` (`competencia_id`),
  ADD KEY `fk_usuarios` (`us_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `competencias`
--
ALTER TABLE `competencias`
  MODIFY `competencia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `detalle_cp`
--
ALTER TABLE `detalle_cp`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_cp`
--
ALTER TABLE `detalle_cp`
  ADD CONSTRAINT `fk_competencias` FOREIGN KEY (`competencia_id`) REFERENCES `competencias` (`competencia_id`),
  ADD CONSTRAINT `fk_usuarios` FOREIGN KEY (`us_id`) REFERENCES `usuarios` (`usuario_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
